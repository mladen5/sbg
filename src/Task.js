import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Moment from 'moment'
import {
  Button, Modal, Col, Layout, message,
} from 'antd'

import Request from './API'


export default class Task extends Component {
  constructor(props) {
    super(props)

    this.state = {
      task: {},
    }

    this.deleteTask = this.deleteTask.bind(this)
  }

  componentDidMount() {
    console.log('match', this.props.match)
    const { id } = this.props.match.params
    Request(`/tasks/${id}`)
      .then(task => this.setState({ task }))
  }

  deleteTask() {
    const { id } = this.props.match.params
    const { task: { name } } = this.state
    return Request(`/tasks/${id}`, {
      method: 'DELETE',
    })
      .then((data) => {
        if (data.message === 'success') {
          message.success(`Task ${name} successfully deleted`)
        } else {
          message.error('Task could NOT be deleted', 10)
        }
      })
  }

  confirmDelete() {
    const { confirm } = Modal

    confirm({
      title: 'Delete this task?',
      okText: 'Yes',
      cancelText: 'No',
      okType: 'danger',
      onOk: () => { this.deleteTask() },
    })
  }

  render() {
    const { task } = this.state
    return (
      <Col span={12}>
        <Layout.Content>
          <h1>{task.name}</h1>
          <p>{task.status}</p>
          <p>
Project:
            {task.project}
          </p>
          <p>
Start time:
            { Moment(task.start_time).toString() }
          </p>
          <p>
End time:
            { Moment(task.end_time).toString() }
          </p>
          <p>
Created by:
            {task.created_by}
          </p>
          <Button
            onClick={this.confirmDelete}
            type="danger"
            icon="delete"
          >
          Delete task
          </Button>
        </Layout.Content>
      </Col>
    )
  }
}

Task.propTypes = {
  match: PropTypes.shape({
    parms: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
}
