import React, { Component } from 'react'
import { Card, Col } from 'antd'
import Request from './API'

export default class User extends Component {
  constructor(props) {
    super(props)

    this.state = {
      user: {},
    }
  }

  componentDidMount() {
    Request('/users/mladen5')
      .then(data => this.setState({ user: data }))
  }


  render() {
    return (
      <Col span={12}>
        <Card title="User Profile" size="small" style={{ width: 600 }}>
          <div className="user-info">
            {displayProperties(this.state.user)}
          </div>
        </Card>
      </Col>
    )
  }
}

// Display and format existing properties
const displayProperties = user => Object.entries(user)
  .map((field) => {
    if (field[1] && typeof field[1] === 'string') {
      return (
        <div key={field}>
          {field[0].replace('_', ' ')}
          <span>
            {field[1]}
          </span>
        </div>

      )
    }
    return null
  })
