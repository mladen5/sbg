import React from 'react'
import PropTypes from 'prop-types'
import { Select, Tag, Col } from 'antd'
import { STATUSES } from './API'

function renderStatusOptions() {
  const { Option } = Select

  return STATUSES.map(status => (
    <Option value={status.value} key={status.value}>
      <Tag color={status.color}>
        {status.value}
      </Tag>
    </Option>
  ))
}

export default function StatusPicker(props) {
  return (
    <Col span={12}>
      <Select
        onChange={status => props.getTasks(status)}
        placeholder="Select status"
        style={{ width: 150 }}
        allowClear
      >
        {renderStatusOptions()}
      </Select>
    </Col>
  )
}

StatusPicker.propTypes = {
  getTasks: PropTypes.func.isRequired,
}
