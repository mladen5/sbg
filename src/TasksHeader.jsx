import React from 'react'
import { Typography } from 'antd'

import StatusPicker from './StatusPicker.jsx'
import User from './User'

export default function TasksHeader(props) {
    return(<div>
        <Typography.Title>CAVATICA Tasks </Typography.Title>
        <StatusPicker getTasks={this.getTasks} />
        <User />
    </div>)
}