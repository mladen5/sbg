module.exports = {
  get: jest.fn(() => Promise.resolve({
    data: [
      {
        href: 'https://cavatica-api.sbgenomics.com/v2/tasks?offset=0&limit=18&status=',
        items: [
          {
            href: 'https://cavatica-api.sbgenomics.com/v2/tasks/c48d2709-c1c3-46f1-af94-049a0dcb9f59',
            id: 'c48d2709-c1c3-46f1-af94-049a0dcb9f59',
            name: 'wgs_gatk4 - HG001-NA12878-50x.vcf',
            project: 'mladen5/copy-of-smart-variant-filtering',
          },
          {
            href: 'https://cavatica-api.sbgenomics.com/v2/tasks/dafb26ba-501c-49dc-b425-ba50f341ac30',
            id: 'dafb26ba-501c-49dc-b425-ba50f341ac30',
            name: 'wgs_gatk4 - HG002-NA24385-50x.vcf',
            project: 'mladen5/copy-of-smart-variant-filtering',
          },
        ],
      },
    ],
  })),
}
