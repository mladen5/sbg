import React from 'react'
import { shallow } from 'enzyme/build'
import App from '../App'


it('renders without crashing', () => {
  jest.spyOn(App.prototype, 'componentDidMount')
  shallow(<App />)
  expect(App.prototype.componentDidMount.mock.calls.length).toBe(1)
})

