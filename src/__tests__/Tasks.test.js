import React from 'react'
import { shallow } from 'enzyme/build'
import Tasks from '../Tasks'

describe('Tasks Component', () => {
  it('renders without crashing', () => {
    const tasks = [{ id: '123', name: 'task name', project: 'project name' }]
    shallow(<Tasks tasks={tasks} total={0} />)
  })
})
