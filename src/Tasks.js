import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Table, Typography } from 'antd'

export default class Tasks extends Component {
  static columns() {
    return [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        render: (name, record) => <Link to={`/task/${record.id}`}>{name}</Link>,
      },
      {
        title: 'ID',
        dataIndex: 'id',
        key: 'id',
        render: id => <Typography.Text code>{id}</Typography.Text>,
      },
      {
        title: 'Project',
        dataIndex: 'project',
        key: 'project',
      },
    ]
  }

  constructor(props) {
    super(props)

    this.state = {
      pageSize: 5,
    }
  }

  pagination() {
    const { pageSize } = this.state
    const { total } = this.props
    return {
      onShowSizeChange: ({ newSize }) => this.setState({ pageSize: newSize }),
      showTotal: (sum, range) => `${range[0]} - ${range[1]} of ${sum} tasks`,
      total,
      pageSize,
      showSizeChanger: true,
      defaultPageSize: 10,
      pageSizeOptions: ['5', '10', '25'],
    }
  }

  render() {
    const { tasks } = this.props
    return (
      <Table
        className="tasks-table"
        pagination={this.pagination()}
        columns={Tasks.columns()}
        dataSource={tasks}
        rowKey="id"
        sorter={(a, b) => a.name.length - b.name.length}
      />
    )
  }
}

Tasks.propTypes = {
  tasks: PropTypes.arrayOf(PropTypes.object).isRequired,
  total: PropTypes.number.isRequired,
}
