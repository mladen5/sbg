const API = 'https://cavatica-api.sbgenomics.com/v2';
const token = 'fe68f9aecfc54234ab077bf86025810e';

function checkStatus(res) {
  const { status } = res;
  if (status >= 200 && status < 300) {
    return res.json()
      .catch(() => { throw new Error('error parasing JSON'); });
  }
  if (status >= 400 && status < 500) {
    throw new Error('Client error');
  }
  if (status >= 500) {
    throw new Error('Server error');
  }
  return res
}

export default function Request(point, params) {
  return fetch(`${API}${point}`, {
    headers: { 'X-SBG-Auth-Token': token },
    'content-type': 'application/json',
    params,
  })
    .then(res => checkStatus(res))
    .then(data => data)
    .catch((e) => { throw new Error(e) })
}

export const STATUSES = [
  { value: 'QUEUED', color: 'blue' },
  { value: 'DRAFT', color: 'grey' },
  { value: 'RUNNING', color: 'orange' },
  { value: 'COMPLETED', color: 'green' },
  { value: 'ABORTED', color: 'pink' },
  { value: 'FAILED', color: 'red' },
];
