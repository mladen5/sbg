import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import {
  Layout, Typography, Row,
} from 'antd'

import './App.css'
import Request from './API'
import Tasks from './Tasks'
import Task from './Task'
import StatusPicker from './StatusPicker'
import User from './User'
import AddProject from './AddProject'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tasks: [],
      total: 0,
    }
    this.getTasks = this.getTasks.bind(this)
  }

  componentDidMount() {
    this.getTasks()
  }

  getTasks(statusName = '') {
    return Request(`/tasks?status=${statusName}`)
      .then((data) => {
        this.setState({ tasks: data.items, total: data.items.length })
      })
  }

  render() {
    const { tasks, total } = this.state
    return (
      <Layout>
        <Row justift="center">
          <header>
            <Typography.Title style={{ textAlign: 'center' }}>CAVATICA Tasks </Typography.Title>
            <StatusPicker getTasks={this.getTasks} />
            <User />
            <AddProject />
          </header>

          <Router>
            <Route
              exact
              path="/"
              render={
                props => <Tasks {...props} tasks={tasks} total={total} />
              }
            />
            <Route path="/task/:id" component={Task} />
          </Router>
        </Row>

      </Layout>
    )
  }
}
