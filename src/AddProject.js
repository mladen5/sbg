import React, { Component } from 'react'
import { Button, Modal, Input, message } from 'antd'
import Request from './API'

export default class AddProject extends Component {

  constructor(props) {
    super(props)
    this.state = {
      visible: false,
      projectTitle: '',
      projectDesc: '',
    }

    this.createNewProject = this.createNewProject.bind(this)
  }

  createNewProject() {
    const params = {
      method: 'POST',
      body: {
        project: this.state.projectTitle,
        billing_group: '9c6fa644-c744-4538-9e29-2abdab4b64a8',
        description: this.state.projectDesc,
      }
    }
    Request('/projects', params)
      .then((data) => {
        if (data) {
          this.setState({ visible: false })
          message.success(`New project ${this.state.projectTitle} added successfully`)
        }
      })
  }

  render() {
    return (
      <div>
        <Button onClick={() => this.setState({ visible: true })}>Add Project</Button>
        <Modal
          title="Add new Project"
          visible={this.state.visible}
          onOk={this.createNewProject}
          onCancel={() => this.setState({ visible: false })}
        >
          <Input
            onChange={e => this.setState({ projectTitle: e.target.value })}
            placeholder="Project Title"
          />
          <Input.TextArea onChange={e => this.setState({ projectDesc: e.target.value })}
            placeholder="Project Description"
          />

        </Modal>
      </div>
    )
  }
}
